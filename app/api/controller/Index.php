<?php
declare (strict_types=1);

namespace app\api\controller;

use app\api\model\Shici;
use app\api\model\Feedback;

class Index
{
    public function index()
    {
        $list = Shici::alias('s')
            ->field('s.*, a.name author,a.dynasty')
            ->join('shici_author a', 's.author_id=a.id')
            ->select();

        return json_encode(['msg' => 'ok', 'list' => $list]);
    }

    public function detail()
    {
        $id = input('id');

        $detail = Shici::alias('s')
            ->field('s.*, a.name author,a.dynasty')
            ->join('shici_author a', 's.author_id=a.id','left')
            ->where('s.id', $id)->find();
        return json_encode(['msg' => $id, 'detail' => $detail]);
    }

    public function search(){
        $kw = input('kw');

        $list = Shici::alias('s')
            ->field('s.*, a.name author,a.dynasty')
            ->join('shici_author a', 's.author_id=a.id')
            ->whereLike('s.title','%'.$kw.'%')
            ->select();
        return json_encode(['msg' => 'ok', 'list' => $list]);
    }

    /**
     *
     */
    public function feedback(){

        $feed_content = input('feed_content');

        $data['feed_content'] = $feed_content;
        Feedback::create($data);
    }
}
