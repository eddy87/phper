<?php
// 事件定义文件
return [
    'bind'      => [
    ],

    'listen'    => [
        'AppInit'  => [],
        'HttpRun'  => [],
        'HttpEnd'  => [],
        'LogLevel' => [],
        'LogWrite' => [],
        // 监听swoole事件，也可以写在swoole.php配置文件中
        'swoole.websocket.Connect'=>[
            \app\listener\WsConnect::class
        ],
        'swoole.websocket.Close'=>[
            \app\listener\WsClose::class
        ],
        // 自定义websocket事件， 在前端发送事件需要到Message，而不是WsMessage
        'swoole.websocket.Message'=>[
            \app\listener\WsMessage::class
        ]
    ],

    'subscribe' => [
    ],
];
