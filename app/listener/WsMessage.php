<?php
declare (strict_types = 1);

namespace app\listener;

use think\swoole\Websocket;

class WsMessage
{
    /**
     * ws接受客户端信息事件监听处理
     *
     * @return mixed
     */
    public function handle($event, Websocket $websocket)
    {
        echo $event.PHP_EOL;
        // 发送广播，除了自己所有人
        $websocket->broadcast()->emit('test','你发的啥');
    }    
}
