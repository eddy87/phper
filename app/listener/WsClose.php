<?php
declare (strict_types = 1);

namespace app\listener;

class WsClose
{
    /**
     * ws 客户端关闭事件监听处理
     *
     * @return mixed
     */
    public function handle($event)
    {
        echo 'close';
    }    
}
