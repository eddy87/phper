<?php
declare (strict_types=1);

namespace app\listener;

use think\swoole\Websocket;

class WsConnect
{
    /**
     * ws连接成功事件监听处理
     * @return mixed
     */
    public function handle($event, Websocket $websocket)
    {
        // $websocket = app(\think\swoole\Websocket::class);  // 等效

    }
}
